﻿using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LeiLaoExpress.UI.MVC.Controllers
{
    public class CargaController : BaseController
    {
		private readonly ICargaService _service;
		private readonly IOportunidadeService _serviceOportunidade;

		public CargaController(ICargaService service, IOportunidadeService serviceOportunidade)
		{
			_service = service;
			_serviceOportunidade = serviceOportunidade;
		}

        // GET: Carga
        public async Task<ActionResult>Index()
        {
			var lista = await _service.GetAllAsync(x=>x.UserId == IdUsuarioLogado);

            return View(lista);
        }

        // GET: Carga/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Carga/Create
        [HttpPost]
        public ActionResult Create(Carga model)
        {
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			model.UserId = IdUsuarioLogado;

			_service.Create(model);

			return RedirectToAction("Index");
        }

        // GET: Carga/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
			var model = await _service.GetByIdAsync(id);

            return View(model);
        }

        // POST: Carga/Edit/5
        [HttpPost]
        public ActionResult Edit(Carga model)
        {
            if (!ModelState.IsValid)
			{
				return View(model);
			}

			_service.Update(model);

			return RedirectToAction("Index");
        }

        // GET: Carga/Delete/5
        public ActionResult Delete(int id)
        {
			_service.Remove(id);

			return RedirectToAction("Index");
        }
    }
}
