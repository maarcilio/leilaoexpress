﻿using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeiLaoExpress.UI.MVC.Controllers
{
	public class MeioTransporteController : BaseController
	{
		private readonly IMeioTransporteService _service;

		public MeioTransporteController(IMeioTransporteService service)
		{
			_service = service;
		}

		// GET: MeioTransporte
		public ActionResult Index()
		{
			return View(_service.GetAll());
		}

		// GET: MeioTransporte/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: MeioTransporte/Create
		[HttpPost]
		public ActionResult Create(MeioTransporte model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			_service.Create(model);

			return RedirectToAction("Index");
		}

		// GET: MeioTransporte/Edit/5
		public ActionResult Edit(int id)
		{
			return View(_service.GetById(id));
		}

		// POST: MeioTransporte/Edit/5
		[HttpPost]
		public ActionResult Edit(MeioTransporte model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			_service.Update(model);

			return RedirectToAction("Index");
		}

		// GET: MeioTransporte/Delete/5
		public ActionResult Delete(int id)
		{
			_service.Remove(id);

			return RedirectToAction("Index");
		}
	}
}
