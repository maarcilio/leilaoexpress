﻿using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using LeiLaoExpress.Infra.Data;
using LeiLaoExpress.Domain.Enum;

namespace LeiLaoExpress.UI.MVC.Controllers
{
    [Authorize]
    public class OpportunityController : AnexoBaseController
    {
        private readonly IOportunidadeService _service;
        private readonly ICargaService _serviceCarga;
        private readonly ILanceService _serviceLance;
        private readonly IFeedbackService _serviceFeedback;

        public OpportunityController(IOportunidadeService service, ICargaService serviceCarga, ILanceService serviceLance,
            IFeedbackService serviceFeedback)
        {
            _service = service;
            _serviceCarga = serviceCarga;
            _serviceLance = serviceLance;
            _serviceFeedback = serviceFeedback;
        }

        // GET: Opportunity
        public async Task<ActionResult> Index()
        {
            var lista = await _service.GetAllAsync();

            ViewBag.IsFornecedor = TipoUsuarioLogado == TypeOfUser.Fornecedor;


            return View(lista);
        }

        public async Task<ActionResult> MinhasOportunidades()
        {
            var lista = await _service.GetAllAsync(x => x.User.Id == IdUsuarioLogado);

            return View("Lista", lista);
        }

        public ActionResult Create()
        {
            ViewBag.CargaId = new SelectList(_serviceCarga.GetAll(x => x.UserId == IdUsuarioLogado), "Id", "Nome");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Oportunidade model, int? CargaId)
        {
            if (!CargaId.HasValue)
                ModelState.AddModelError("CargaId", "Escolha uma carga.");

            if (!ModelState.IsValid)
            {
                ViewBag.CargaId = new SelectList(_serviceCarga.GetAll(x => x.UserId == IdUsuarioLogado), "Id", "Nome", CargaId);

                return View(model);
            }

            model.UserId = IdUsuarioLogado;

            var carga = _serviceCarga.GetById(CargaId.Value);

            carga.Oportunidades.Add(model);

            _serviceCarga.Update(carga);

            return RedirectToAction("MinhasOportunidades");
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var model = await _service.GetByIdAsync(Id);

            ViewBag.CargaId = new SelectList(_serviceCarga.GetAll(x => x.UserId == IdUsuarioLogado), "Id", "Nome", model.Cargas.FirstOrDefault().Id);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Oportunidade model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _service.UpdateAsync(model);

            return RedirectToAction("MinhasOportunidades");
        }

        public async Task<ActionResult> Details(int Id)
        {
            var model = await _service.GetByIdAsync(Id);
            return View(model);
        }

        public ActionResult Delete(int Id)
        {
            _service.Remove(Id);

            return RedirectToAction("MinhasOportunidades");
        }

        public ActionResult DetalharOportunidade(int Id)
        {
            var model = _service.GetById(Id);

            ViewBag.IsFornecedor = TipoUsuarioLogado == TypeOfUser.Fornecedor;
            
            if (model.DataColeta.AddDays(3).Date < DateTime.Now.Date)
            {
                if (!model.Lances.Any(x => x.Escolhida))
                {
                    Lance lance = model.Lances.OrderBy(x => x.Valor).ThenByDescending(x => x.DataColeta).ThenByDescending(x => x.DataEntrega).FirstOrDefault();

                    if (lance != null)
                    {
                        lance.Escolhida = true;

                        _service.Update(model);
                    }
                }

            }

            ViewBag.Encerrada = model.Lances.Any(x => x.Escolhida);
            ViewBag.AtivarFeedback = !_serviceFeedback.GetAll(x => x.Oportunidade.Id == model.Id).Any();
            return View(model);
        }

        public ActionResult SelecionarServico(int id)
        {
            var lance = _serviceLance.GetById(id);

            if (lance == null)
                return HttpNotFound();

            var lanceEscolhido = _serviceLance.GetAll(x => x.Escolhida && x.Oportunidade.Id == lance.Oportunidade.Id).FirstOrDefault();


            if (lanceEscolhido != null)
            {

                lanceEscolhido.Escolhida = false;

                _serviceLance.Update(lanceEscolhido);
            }
            lance.Escolhida = true;

            _serviceLance.Update(lance);

            return RedirectToAction("DetalharOportunidade", new { id = lance.Oportunidade.Id });
        }

    }
}