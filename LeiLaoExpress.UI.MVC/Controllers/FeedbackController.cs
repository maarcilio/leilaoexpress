﻿using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeiLaoExpress.UI.MVC.Controllers
{
    public class FeedbackController : Controller
    {
        private readonly IFeedbackService _service;
        private readonly IOportunidadeService _serviceOportunidade;
        private readonly IFornecedorService _serviceFornecedor;

        public FeedbackController(IFeedbackService service, IOportunidadeService serviceOportunidade,
            IFornecedorService serviceFornecedor)
        {
            _service = service;
            _serviceOportunidade = serviceOportunidade;
            _serviceFornecedor = serviceFornecedor;
        }

        // GET: Feedback
        public ActionResult Index(string id)
        {
            var lista = _service.GetAll(x => x.Fornecedor.Id == id);

            ViewBag.Nome = _serviceFornecedor.GetById(id).Nome;

            return PartialView(lista);
        }

        // GET: Feedback/Create
        public PartialViewResult ModalCreate(int idOportunidade)
        {
            var oportunidade = _serviceOportunidade.GetById(idOportunidade);

            var fornecedorId = oportunidade.Lances.FirstOrDefault(x => x.Escolhida).Fornecedor.UserId;

            Feedback model = new Feedback { OportunidadeId = idOportunidade, FornecedorId = fornecedorId };
            return PartialView(model);
        }

        // GET: Feedback/Create
        [HttpPost]
        public ActionResult ModalCreate(Feedback model)
        {
            _service.Create(model);

            return RedirectToAction("DetalharOportunidade", "Opportunity", new { id = model.OportunidadeId });
        }

        // GET: Feedback/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Feedback/Create
        [HttpPost]
        public ActionResult Create(Feedback model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _service.Create(model);


            return RedirectToAction("Index");
        }

        // GET: Feedback/Edit/5
        public ActionResult Edit(int id)
        {

            return View(_service.GetById(id));
        }

        // POST: Feedback/Edit/5
        [HttpPost]
        public ActionResult Edit(Feedback model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _service.Update(model);


            return RedirectToAction("Index");
        }

        // GET: Feedback/Delete/5
        public ActionResult Delete(int id)
        {
            _service.Remove(id);

            return RedirectToAction("Index");
        }

    }
}
