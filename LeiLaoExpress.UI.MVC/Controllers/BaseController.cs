﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using LeiLaoExpress.Domain.Enum;

namespace LeiLaoExpress.UI.MVC.Controllers
{
    public class BaseController : Controller
    {
		public string IdUsuarioLogado
		{
			get
			{
				return User.Identity.IsAuthenticated ? User.Identity.GetUserId() : string.Empty;
			}
		}

		public TypeOfUser? TipoUsuarioLogado
		{
			get
			{
				if(!User.Identity.IsAuthenticated)
					return null;
				else
				{
					var identity = (ClaimsIdentity)User.Identity;

					var typeOfUser = identity.Claims.FirstOrDefault(c => c.Type == "TypeOfUser");
					return typeOfUser.Value == TypeOfUser.Cliente.ToString() ? TypeOfUser.Cliente : TypeOfUser.Fornecedor;
				}
			}
		}
    }
}