﻿using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LeiLaoExpress.UI.MVC.Controllers
{
	public class LanceController : BaseController
	{
		private readonly ILanceService _service;
		private readonly IMeioTransporteService _serviceMeioTransporte;

		public LanceController(ILanceService service, IMeioTransporteService serviceMeioTransporte)
		{
			_service = service;
			_serviceMeioTransporte = serviceMeioTransporte;
		}

		// GET: Lance
		public async Task<ActionResult> Index()
		{
			var lista = await _service.GetAllAsync(x => x.Fornecedor.UserId == IdUsuarioLogado);

			return View(lista);
		}

		public ActionResult Create(int idOportunidade)
		{
			ViewBag.idOportunidade = idOportunidade;

			ViewBag.MeioTransporteId = new SelectList(_serviceMeioTransporte.GetAll(), "Id", "Nome");

			return View();
		}

		[HttpPost]
		public ActionResult Create(Lance model)
		{
			if (!ModelState.IsValid)
			{
				ViewBag.idOportunidade = model.OportunidadeId;

				ViewBag.MeioTransporteId = new SelectList(_serviceMeioTransporte.GetAll(), "Id", "Nome", model.MeioTransporteId);

				return View(model);
			}

			model.FornecedorId = IdUsuarioLogado;

			_service.Create(model);

			return RedirectToAction("Index", "Opportunity");
		}

		public ActionResult Edit(int Id)
		{
			var model = _service.GetById(Id);

			ViewBag.idOportunidade = model.Oportunidade.Id;

			ViewBag.MeioTransporteId = new SelectList(_serviceMeioTransporte.GetAll(), "Id", "Nome", model.MeioTransporte.Id);

			return View(model);
		}

		[HttpPost]
		public ActionResult Edit(Lance model)
		{
			if (!ModelState.IsValid)
			{
				ViewBag.idOportunidade = model.OportunidadeId;

				ViewBag.MeioTransporteId = new SelectList(_serviceMeioTransporte.GetAll(), "Id", "Nome", model.MeioTransporteId);

				return View(model);
			}

			model.FornecedorId = IdUsuarioLogado;

			_service.Update(model);

			return RedirectToAction("Index");
		}

		public ActionResult Delete(int Id)
		{
			_service.Remove(Id);

			return RedirectToAction("Index");
		}

		public PartialViewResult LanceModal(int OportunidadeId)
		{
			ViewBag.OportunidadeId = OportunidadeId;

			ViewBag.MeioTransporteId = new SelectList(_serviceMeioTransporte.GetAll(), "Id", "Nome");

			return PartialView();
		}

		[HttpPost]
		public ActionResult LanceModal(Lance model)
		{
			if (!ModelState.IsValid)
			{
				ViewBag.OportunidadeId = model.OportunidadeId;

				ViewBag.MeioTransporteId = new SelectList(_serviceMeioTransporte.GetAll(), "Id", "Nome");

				return PartialView(model);
			}

			model.FornecedorId = IdUsuarioLogado;

			_service.Create(model);

			return RedirectToAction("DetalharOportunidade", "Opportunity", new { Id = model.OportunidadeId });
		}
	}
}