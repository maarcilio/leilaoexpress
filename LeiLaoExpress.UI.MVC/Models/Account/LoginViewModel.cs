﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeiLaoExpress.UI.MVC.Models.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Email", ResourceType = typeof(Inputs))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "ErrorEmailFormat", ErrorMessageResourceType = typeof(Messages))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Inputs))]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(Inputs))]
        public bool RememberMe { get; set; }
    }
}