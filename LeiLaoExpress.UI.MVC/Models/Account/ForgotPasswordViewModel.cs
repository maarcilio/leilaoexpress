﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeiLaoExpress.UI.MVC.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "ErrorEmailFormat", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Email", ResourceType = typeof(Inputs))]
        public string Email { get; set; }
    }
}