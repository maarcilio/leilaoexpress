﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeiLaoExpress.UI.MVC.Models.Account
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Email", ResourceType = typeof(Inputs))]
        public string Email { get; set; }
    }
}