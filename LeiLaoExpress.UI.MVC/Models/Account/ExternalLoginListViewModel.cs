﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeiLaoExpress.UI.MVC.Models.Account
{
    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
}