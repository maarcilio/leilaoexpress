﻿using LeiLaoExpress.Common.Resource;
using LeiLaoExpress.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeiLaoExpress.UI.MVC.Models.Account
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = null, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Name", ResourceType = typeof(Inputs))]
        [StringLength(100, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "ErroStringLength", MinimumLength = 6)]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "ErrorEmailFormat", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Email", ResourceType = typeof(Inputs))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "TypeOfUser", ResourceType = typeof(Inputs))]
        public TypeOfUser TypeOfUser { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(100, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "ErroStringLength", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Inputs))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Inputs))]
        [Compare("Password", ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "ErrorConfirmPassword")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "CNPJ", ResourceType = typeof(Text))]
        public string CNPJ { get; set; }
    }
}