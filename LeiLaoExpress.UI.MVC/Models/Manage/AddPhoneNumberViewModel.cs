﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeiLaoExpress.UI.MVC.Models.Manage
{
    public class AddPhoneNumberViewModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [Phone(ErrorMessageResourceName = "ErrorPhoneFormat", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "PhoneNumber", ResourceType = typeof(Inputs))]
        public string Number { get; set; }
    }
}