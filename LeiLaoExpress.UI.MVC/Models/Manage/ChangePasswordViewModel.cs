﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeiLaoExpress.UI.MVC.Models.Manage
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [DataType(DataType.Password)]
        [Display(Name = "CurrentPassword", ResourceType = typeof(Inputs))]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(100, ErrorMessageResourceName = "ErroStringLength",ErrorMessageResourceType=typeof(Messages), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "NewPassword", ResourceType = typeof(Inputs))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmNewPassword")]
        [Compare("NewPassword", ErrorMessageResourceName = "ErrorConfirmNewPassword", ErrorMessageResourceType=typeof(Messages))]
        public string ConfirmPassword { get; set; }
    }
}