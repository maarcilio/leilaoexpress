﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using LeiLaoExpress.Domain.Enum;

namespace LeiLaoExpress.UI.MVC.Models.Client
{
    public class ManageClientViewModel
    {
        [Required(ErrorMessage = null, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(255, ErrorMessage = null, ErrorMessageResourceName = "ErroStringLength", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Name", ResourceType = typeof(Inputs))]
        public string Name { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(255, ErrorMessage = null, ErrorMessageResourceName = "ErroStringLength", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Adress", ResourceType = typeof(Inputs))]
        public string Adress { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(12, ErrorMessage = null, ErrorMessageResourceName = "ErroStringLength", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "PhoneNumber", ResourceType = typeof(Inputs))]
        public string Phone { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(14, ErrorMessage = null, ErrorMessageResourceName = "ErroStringLength", ErrorMessageResourceType = typeof(Messages))]
        public string CNPJ { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(11, ErrorMessage = null, ErrorMessageResourceName = "ErroStringLength", ErrorMessageResourceType = typeof(Messages))]
        public string CPF { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Messages))]
        [StringLength(12, ErrorMessage = null, ErrorMessageResourceName = "ErroStringLength", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "TypeOfClient", ResourceType = typeof(Inputs))]
        public TypeOfClient TypeOfClient { get; set; }
    }
}