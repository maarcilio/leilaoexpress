﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LeiLaoExpress.UI.MVC.Startup))]
namespace LeiLaoExpress.UI.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
