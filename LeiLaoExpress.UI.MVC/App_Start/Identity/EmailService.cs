﻿using LeiLaoExpress.Common.Resource;
using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;

namespace LeiLaoExpress.UI.MVC.App_Start.Identity
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            //return ConfigSendGridasync(message);
            return SendMail(message);
        }

        // Implementação de e-mail manual
        private Task SendMail(IdentityMessage message)
        {

            var text = HttpUtility.HtmlEncode(message.Body);

            var msg = new MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["UserName"], Text.NameSystem);
            msg.To.Add(new MailAddress(message.Destination));
            msg.Subject = message.Subject;
            msg.IsBodyHtml = true;
            msg.Body = message.Body;
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["SMTP"], Convert.ToInt32(ConfigurationManager.AppSettings["Porta"]));
            var credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserName"],
                ConfigurationManager.AppSettings["Pasword"]);
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);

            return Task.FromResult(0);
        }
    }
}