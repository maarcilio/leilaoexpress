﻿using LeiLaoExpress.Common.Resource;
using LeiLaoExpress.Domain.Model;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace LeiLaoExpress.UI.MVC.App_Start.Identity
{
    public class ApplicationSignInManager : SignInManager<UserApp, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(UserApp user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        public override Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            Task<SignInStatus> result = base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);

            //Enviar notificação ao usuário avisando que sua conta sofre 5 tentativas de acesso
            if (result.Result == SignInStatus.LockedOut)
            {
                var data = UserManager.GetLockoutEndDateAsync(UserManager.Users.FirstOrDefault(x => x.UserName == userName).Id);

                if (data.Result.AddSeconds(7) <= DateTime.Now)
                {
                    Common.Services.EmailService emailService = new Common.Services.EmailService();

                    emailService.SendEmail(userName, Text.SubjectBlockAcessYouAccount, Text.BodyBlockAcessYouAccount);
                }

            }

            return result;
        }
    }
}