﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using LeiLaoExpress.UI.MVC.Models;
using LeiLaoExpress.UI.MVC.App_Start.Identity;
using LeiLaoExpress.Domain.Model;
using LeiLaoExpress.Infra.Data;
using LeiLaoExpress.Common.Resource;

namespace LeiLaoExpress.UI.MVC
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<UserApp>
    {
        public ApplicationUserManager(IUserStore<UserApp> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<UserApp>(context.Get<LeilaoExpressContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<UserApp>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<UserApp>
            {
                MessageFormat = Messages.YourSecurityCodeIs
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<UserApp>
            {
                Subject = Messages.SecurityCode,
                BodyFormat = Messages.YourSecurityCodeIs
            });
            manager.EmailService = new EmailService();
            //manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<UserApp>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }


        
    }
}
