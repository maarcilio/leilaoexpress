﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Repository
{
    public class ClienteRepository : RepositoryBase<Cliente>, IClienteRepository
    {
    }
}
