﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Infra.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Repository
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity>, IDisposable where TEntity : class
    {
        protected LeilaoExpressContext _db = new LeilaoExpressContext();

        public void Create(TEntity model)
        {
            _db.Set<TEntity>().Add(model);
            _db.SaveChanges();
        }

        public void CreateAsync(TEntity model)
        {
            _db.Set<TEntity>().Add(model);
            _db.SaveChangesAsync();
        }

        public void Update(TEntity model)
        {
            _db.Entry(model).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void UpdateAsync(TEntity model)
        {
            _db.Entry(model).State = EntityState.Modified;
            _db.SaveChangesAsync();
        }

        public void Remove(TEntity model)
        {
            _db.Set<TEntity>().Remove(model);
            _db.SaveChanges();
        }

        public void RemoveAsync(TEntity model)
        {
            _db.Set<TEntity>().Remove(model);
            _db.SaveChangesAsync();
        }

        public TEntity GetById(int Id)
        {
            return _db.Set<TEntity>().Find(Id);
        }

        public async Task<TEntity> GetByIdAsync(int Id)
        {
            return await _db.Set<TEntity>().FindAsync(Id);
        }

        public TEntity GetById(Guid Id)
        {
            return _db.Set<TEntity>().Find(Id);
        }

        public async Task<TEntity> GetByIdAsync(Guid Id)
        {
            return await _db.Set<TEntity>().FindAsync(Id);
        }

        public TEntity GetById(string Id)
        {
            return _db.Set<TEntity>().Find(Id);
        }

        public async Task<TEntity> GetByIdAsync(string Id)
        {
            return await _db.Set<TEntity>().FindAsync(Id);
        }

        public ICollection<TEntity> GetAll()
        {
            return _db.Set<TEntity>().ToList<TEntity>();
        }

        public async Task<ICollection<TEntity>> GetAllAsync()
        {
            return await _db.Set<TEntity>().ToListAsync<TEntity>();
        }

        public ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return _db.Set<TEntity>().Where(predicate).ToList<TEntity>();
        }

        public async Task<ICollection<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _db.Set<TEntity>().Where(predicate).ToListAsync<TEntity>();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

    }
}
