﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data.ClassMap
{
    public class LanceMap : EntityTypeConfiguration<Lance>
    {

        public LanceMap()
        {
            ToTable("Lance");

            HasKey(x => x.Id);

			Property(x => x.Observacao)
				.HasColumnName("Observacao")
				.IsOptional();

			Property(x => x.Escolhida)
				.HasColumnName("Escolhida")
				.IsRequired();

			HasRequired(x => x.Oportunidade).WithMany(x => x.Lances)
			   .HasForeignKey(x => x.OportunidadeId);

			HasRequired(x => x.Fornecedor).WithMany(x => x.Lances)
			   .HasForeignKey(x => x.FornecedorId);

			HasRequired(x => x.MeioTransporte).WithMany(x => x.Lances)
			   .HasForeignKey(x => x.MeioTransporteId);

        }

    }
}
