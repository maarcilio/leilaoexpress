﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data.ClassMap
{
    public class CargaMap : EntityTypeConfiguration<Carga>
    {
        public CargaMap()
        {
            this.HasKey(x => x.Id);

            Property(x => x.Nome)
                .HasColumnName("Nome")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Peso)
                .HasColumnName("Peso");

            Property(x => x.Dimensao)
                .HasColumnName("Dimensao")
                .HasMaxLength(60);

            Property(x => x.Fragil)
                .HasColumnName("Fragil");

			HasRequired(x => x.User).WithMany(x => x.Cargas)
				.HasForeignKey(x => x.UserId);

            HasMany(x => x.Anexos)
                .WithMany(x => x.Cargas)
                    .Map(c =>
                    {
                        c.MapLeftKey("CargaId");
                        c.MapRightKey("AnexoId");
                        c.ToTable("CargaAnexo");
                    });



            ToTable("Carga");
        }
    }
}
