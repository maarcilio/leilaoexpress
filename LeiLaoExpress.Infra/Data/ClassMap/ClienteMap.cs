﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data.ClassMap
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            HasKey(x => x.UserId);

            Property(x => x.Nome)
                .HasColumnName("Nome")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Telefone)
                .HasColumnName("Telefone")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.Endereco)
                .HasColumnName("Endereco")
                .IsOptional();

            Property(x => x.CGC)
                .HasColumnName("CGC")
                .HasMaxLength(16)
                .IsOptional();

            HasRequired(ad => ad.User)
                    .WithOptional(s => s.Cliente);
        }
    }
}
