﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data.ClassMap
{
    public class AnexoMap : EntityTypeConfiguration<Anexo>
    {
        public AnexoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .HasMaxLength(255)
                .HasColumnName("Nome")
                .IsRequired();

            Property(x => x.Caminho)
                .HasColumnName("Caminho")
                .HasMaxLength(255)
                .IsRequired();


            ToTable("Anexo");
        }
    }
}
