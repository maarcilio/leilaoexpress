﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data.ClassMap
{
    public class OportunidadeMap : EntityTypeConfiguration<Oportunidade>
    {
        public OportunidadeMap()
        {
            ToTable("Oportunidade");

            HasKey(x => x.Id);

            Property(x => x.Origem).HasColumnName("Origem")
                .HasMaxLength(255).IsRequired();

            Property(x => x.Destino).HasColumnName("Destino")
                .HasMaxLength(255).IsRequired();

            Property(x => x.DataColeta).HasColumnName("DataColeta")
                .IsRequired();

            Property(x => x.DataEntrega).HasColumnName("DataEntrega")
                .IsRequired();

            HasRequired(x => x.User).WithMany(x => x.Oportunidades)
                .HasForeignKey(x=>x.UserId);


            HasMany(x => x.Anexos)
               .WithMany(x => x.Oportunidades)
                   .Map(c =>
                   {
                       c.MapLeftKey("OportunidadeId");
                       c.MapRightKey("AnexoId");
                       c.ToTable("OportunidadeAnexo");
                   });

            HasMany(x => x.Cargas)
                .WithMany(x => x.Oportunidades)
                    .Map(c =>
                    {
                        c.MapLeftKey("OportunidadeId");
                        c.MapRightKey("CargaId");
                        c.ToTable("OportunidadeCarga");
                    });
        }
    }
}
