﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data.ClassMap
{
    public class FornecedorMap : EntityTypeConfiguration<Fornecedor>
    {

        public FornecedorMap()
        {
            ToTable("Fornecedor");

            HasKey(x => x.UserId);

            Property(x => x.Nome)
                .HasColumnName("Nome")
                .HasMaxLength(25)
                .IsRequired();

            Property(x => x.Telefone)
                .HasColumnName("Telefone")
                .HasMaxLength(11)
                .IsOptional();

            Property(x => x.Endereco)
                .HasColumnName("Endereco")
                .HasMaxLength(255);

            Property(x => x.CNPJ)
                .HasColumnName("CNPJ")
                .HasMaxLength(16)
                .IsRequired();

            HasRequired(ad => ad.User)
                    .WithOptional(s => s.Fornecedor);

            HasMany(x => x.Anexos)
                .WithMany(x => x.Fornecedores)
                    .Map(c =>
                    {
                        c.MapLeftKey("FornecedorId");
                        c.MapRightKey("AnexoId");
                        c.ToTable("FornecedorAnexo");
                    });
        }

    }
}
