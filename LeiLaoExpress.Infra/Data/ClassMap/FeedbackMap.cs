﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data.ClassMap
{
	public class FeedbackMap : EntityTypeConfiguration<Feedback>
	{
		public FeedbackMap()
		{
			ToTable("Feedback");

			HasKey(x => x.Id);

			Property(x => x.Comentario)
				.HasColumnName("Comentario")
				.IsOptional();

			Property(x => x.Classificacao)
				.HasColumnName("Classificacao")
				.IsRequired();

			HasRequired(x => x.Oportunidade).WithMany(x => x.Feedback)
			   .HasForeignKey(x => x.OportunidadeId);

			HasRequired(x => x.Fornecedor).WithMany(x => x.Feedback)
			   .HasForeignKey(x => x.FornecedorId);
		}
	}
}
