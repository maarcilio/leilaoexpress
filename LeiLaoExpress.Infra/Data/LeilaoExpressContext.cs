﻿using LeiLaoExpress.Domain.Model;
using LeiLaoExpress.Infra.Data.ClassMap;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;

namespace LeiLaoExpress.Infra.Data
{
    public class LeilaoExpressContext : IdentityDbContext<UserApp>
    {
        static LeilaoExpressContext()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<LeilaoExpressContext>());
        }

        public LeilaoExpressContext()
            : base("LeilaoExpressDB")
        {

        }

        #region [ Tabelas (DbSet) ]

        public DbSet<Anexo> Anexo { get; set; }
        public DbSet<Carga> Carga { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Fornecedor> Fornecedor { get; set; }
        public DbSet<Lance> Lance{ get; set; }
        public DbSet<Oportunidade> Oportuinidade { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            #region [ Add Mapeamneto de Classes ]

            modelBuilder.Configurations.Add(new AnexoMap());
            modelBuilder.Configurations.Add(new CargaMap());
            modelBuilder.Configurations.Add(new ClienteMap());
            modelBuilder.Configurations.Add(new FornecedorMap());
			modelBuilder.Configurations.Add(new LanceMap());
			modelBuilder.Configurations.Add(new FeedbackMap());

            #endregion

            base.OnModelCreating(modelBuilder);

            #region [ Custumizando Nomes Tabelas]

            modelBuilder.Entity<IdentityUser>()
            .ToTable("Users")
            .Property(p => p.Id);

            modelBuilder.Entity<UserApp>()
                .ToTable("Users")
                .Property(p => p.Id);

            modelBuilder.Entity<IdentityUserRole>()
                .ToTable("UserRoles");

            modelBuilder.Entity<IdentityUserLogin>()
                .ToTable("Logins");

            modelBuilder.Entity<IdentityUserClaim>()
                .ToTable("Claims");

            modelBuilder.Entity<IdentityRole>()
                .ToTable("Roles");

            #endregion
        }

        public override Task<int> SaveChangesAsync()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("CreateDate") != null && entry.Entity.GetType().GetProperty("DataModification") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataModification").CurrentValue = DateTime.Now;
                    entry.Property("CreateDate").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataModification").CurrentValue = DateTime.Now;
                    entry.Property("CreateDate").IsModified = false;
                }
            }
            return base.SaveChangesAsync();
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("CreateDate") != null && entry.Entity.GetType().GetProperty("DataModification") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataModification").CurrentValue = DateTime.Now;
                    entry.Property("CreateDate").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataModification").CurrentValue = DateTime.Now;
                    entry.Property("CreateDate").IsModified = false;
                }
            }

            return base.SaveChanges();
        }

        public static LeilaoExpressContext Create()
        {
            return new LeilaoExpressContext();
        }

    }
}
