﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeiLaoExpress.Domain.Model;

namespace LeilaoExpress.UnitTest
{
    /// <summary>
    /// Summary description for FornecedorTest
    /// </summary>
    [TestClass]
    public class FornecedorTest
    {
        public FornecedorTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #region Util

        public static bool ValidaCNPJ(string vrCNPJ)
        {

            string CNPJ = vrCNPJ.Replace(".", "");
            CNPJ = CNPJ.Replace("/", "");
            CNPJ = CNPJ.Replace("-", "");

            int[] digitos, soma, resultado;
            int nrDig;
            string ftmt;
            bool[] CNPJOk;

            ftmt = "6543298765432";
            digitos = new int[14];
            soma = new int[2];
            soma[0] = 0;
            soma[1] = 0;
            resultado = new int[2];
            resultado[0] = 0;
            resultado[1] = 0;
            CNPJOk = new bool[2];
            CNPJOk[0] = false;
            CNPJOk[1] = false;

            try
            {
                for (nrDig = 0; nrDig < 14; nrDig++)
                {
                    digitos[nrDig] = int.Parse(
                     CNPJ.Substring(nrDig, 1));
                    if (nrDig <= 11)
                        soma[0] += (digitos[nrDig] *
                        int.Parse(ftmt.Substring(
                          nrDig + 1, 1)));
                    if (nrDig <= 12)
                        soma[1] += (digitos[nrDig] *
                        int.Parse(ftmt.Substring(
                          nrDig, 1)));
                }

                for (nrDig = 0; nrDig < 2; nrDig++)
                {
                    resultado[nrDig] = (soma[nrDig] % 11);
                    if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                        CNPJOk[nrDig] = (
                        digitos[12 + nrDig] == 0);

                    else
                        CNPJOk[nrDig] = (
                        digitos[12 + nrDig] == (
                        11 - resultado[nrDig]));

                }

                return (CNPJOk[0] && CNPJOk[1]);

            }
            catch
            {
                return false;
            }

        }

        #endregion

        [TestMethod]
        public void Fornecedor_Nome_Valido()
        {
            Fornecedor fornecedor = new Fornecedor();

            fornecedor.Nome = "Marcilio";

            Assert.IsFalse(string.IsNullOrEmpty(fornecedor.Nome));
        }

        [TestMethod]
        public void Fornecedor_CNPJ_Valido()
        {
            Fornecedor fornecedor = new Fornecedor();

            fornecedor.CNPJ = "00.000.000/0001-91";

            var response = ValidaCNPJ(fornecedor.CNPJ);

            Assert.IsTrue(response);
        }

        [TestMethod]
        public void Fornecedor_Endereco_Valido()
        {
            Fornecedor fornecedor = new Fornecedor();

            fornecedor.Endereco = "Rua Olinto Meireles";

            Assert.IsFalse(string.IsNullOrEmpty(fornecedor.Endereco));
        }

        [TestMethod]
        public void Fornecedor_Telefone_Valido()
        {
            Fornecedor fornecedor = new Fornecedor();

            fornecedor.Telefone = "33333333";

            Assert.IsFalse(string.IsNullOrEmpty(fornecedor.Telefone));
        }
    }


}
