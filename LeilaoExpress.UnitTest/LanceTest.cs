﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeiLaoExpress.Domain.Model;

namespace LeilaoExpress.UnitTest
{
    /// <summary>
    /// Summary description for LanceTest
    /// </summary>
    [TestClass]
    public class LanceTest
    {
        public LanceTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void Validar_Lance_Cadastrada_e_Valida()
        {
            Lance model = new Lance();

            model.DataColeta = DateTime.Now;

            model.DataEntrega = DateTime.Now.AddDays(10);

            Assert.IsTrue(model.DataColeta < model.DataEntrega);
        }

        [TestMethod]
        public void Validar_Data_de_Coleta_e_Valida()
        {
            Lance model = new Lance();

            model.DataColeta = DateTime.Now;

            Assert.IsTrue(model.DataColeta == DateTime.Now);

        }

        [TestMethod]
        public void Data_de_Entrega_e_Valida()
        {
            Lance model = new Lance();

            model.DataEntrega = DateTime.Now;

            Assert.IsTrue(model.DataEntrega == DateTime.Now);

        }
    }
}
