﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeiLaoExpress.Domain.Model;

namespace LeilaoExpress.UnitTest
{
    /// <summary>
    /// Summary description for OportunidadeTest
    /// </summary>
    [TestClass]
    public class OportunidadeTest
    {
        public OportunidadeTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Validar_Oportunidade_Cadastrada_e_Valida()
        {
            Oportunidade oportunidade = new Oportunidade();

            oportunidade.DataColeta = DateTime.Now;

            oportunidade.DataEntrega = DateTime.Now.AddDays(10);

            Assert.IsTrue(oportunidade.DataColeta < oportunidade.DataEntrega);
        }

        [TestMethod]
        public void Validar_Origem_Opotunidade()
        {
            Oportunidade oportunidade = new Oportunidade();

            oportunidade.Origem = "Rua Dona Luiza";

            Assert.IsFalse(string.IsNullOrEmpty(oportunidade.Origem));
        }

        [TestMethod]
        public void Validar_Destino_Opotunidade()
        {
            Oportunidade oportunidade = new Oportunidade();

            oportunidade.Destino = "Rua Dona Luiza";

            Assert.IsFalse(string.IsNullOrEmpty(oportunidade.Destino));
        }

        [TestMethod]
        public void Validar_Data_de_Coleta_e_Valida()
        {
            Oportunidade oportunidade = new Oportunidade();

            oportunidade.DataColeta = DateTime.Now;

            Assert.IsTrue(oportunidade.DataColeta == DateTime.Now);

        }

        [TestMethod]
        public void Validar_Data_de_Entrega_e_Valida()
        {
            Oportunidade oportunidade = new Oportunidade();

            oportunidade.DataEntrega = DateTime.Now;

            Assert.IsTrue(oportunidade.DataEntrega == DateTime.Now);

        }
    }
}
