﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeiLaoExpress.Domain.Model;

namespace LeilaoExpress.UnitTest
{
    /// <summary>
    /// Summary description for CargaTest
    /// </summary>
    [TestClass]
    public class CargaTest
    {
        public CargaTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Cadastro_De_Carga_Verificar_Campos_Obrigatrorios_Nome()
        {
            Carga carga = new Carga();

            carga.Nome = string.Empty;

            Assert.IsTrue(string.IsNullOrEmpty(carga.Nome));
        }

        [TestMethod]
        public void Cadastro_De_Carga_Verificar_Campos_Obrigatrorios_Peso()
        {
            Carga carga = new Carga();

            carga.Peso = 5;

            Assert.IsTrue(carga.Peso > 0);
        }

        [TestMethod]
        public void Cadastro_De_Carga_Verificar_Campos_Obrigatrorios_Fragil()
        {
            Carga carga = new Carga();

            carga.Fragil= true;

            Assert.IsTrue(carga.Fragil);
        }

        [TestMethod]
        public void Cadastro_De_Carga_Verificar_Campos_Obrigatrorios_Dimensao()
        {
            Carga carga = new Carga();

            carga.Dimensao = string.Empty;

            Assert.IsTrue(string.IsNullOrEmpty(carga.Dimensao));
        }


    }
}
