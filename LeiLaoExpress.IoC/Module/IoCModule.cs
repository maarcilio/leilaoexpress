﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Infra.Data;
using LeiLaoExpress.Infra.Repository;
using LeiLaoExpress.Service;
using Ninject;
using Ninject.Modules;

namespace LeiLaoExpress.IoC
{
    public class IoCModule : NinjectModule
    {
        public static void LoadKernel(IKernel kernel)
        {
            #region [ Context ]

            kernel.Bind<LeilaoExpressContext>().ToSelf();

            #endregion

            #region [ Repository ]

            kernel.Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            kernel.Bind<IOportunidadeRepository>().To<OportunidadeRepository>();
            kernel.Bind<IClienteRepository>().To<ClienteRepository>();
            kernel.Bind<IFornecedorRepository>().To<FornecedorRepository>();
            kernel.Bind<ICargaRepository>().To<CargaRepository>();
			kernel.Bind<IAnexoRepository>().To<AnexoRepository>();
            kernel.Bind<ILanceRepository>().To<LanceRepository>();
            kernel.Bind<IFeedbackRepository>().To<FeedbackRepository>();
			kernel.Bind<IMeioTransporteRepository>().To<MeioTransporteRepository>();

            #endregion

            #region [ Service ]

            kernel.Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            kernel.Bind<IOportunidadeService>().To<OportunidadeService>();
            kernel.Bind<IClienteService>().To<ClienteService>();
            kernel.Bind<IFornecedorService>().To<FornecedorService>();
            kernel.Bind<ICargaService>().To<CargaService>();
			kernel.Bind<IAnexoService>().To<AnexoService>();
            kernel.Bind<ILanceService>().To<LanceService>();
            kernel.Bind<IFeedbackService>().To<FeedbackService>();
			kernel.Bind<IMeioTransporteService>().To<MeioTransporteService>();

            #endregion  

        }

        public override void Load()
        {
            throw new System.NotImplementedException();
        }
    }
}
