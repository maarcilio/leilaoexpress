﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Contract.Services
{
    public interface IClienteService : IDisposable, IServiceBase<Cliente>
    {
        
    }
}
