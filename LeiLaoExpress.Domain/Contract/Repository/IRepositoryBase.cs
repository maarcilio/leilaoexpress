﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Contract.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Create(TEntity model);

        void CreateAsync(TEntity model);

        void Update(TEntity model);

        void UpdateAsync(TEntity model);

        void Remove(TEntity model);

        void RemoveAsync(TEntity model);

        TEntity GetById(int Id);

        Task<TEntity> GetByIdAsync(int Id);

        TEntity GetById(Guid Id);

        Task<TEntity> GetByIdAsync(Guid Id);

        TEntity GetById(string Id);

        Task<TEntity> GetByIdAsync(string Id);

        ICollection<TEntity> GetAll();

        Task<ICollection<TEntity>> GetAllAsync();

        ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        Task<ICollection<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate);

        void Dispose();
    }
}
