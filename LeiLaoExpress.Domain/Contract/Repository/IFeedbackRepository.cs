﻿using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Contract.Repository
{
	public interface IFeedbackRepository : IDisposable, IRepositoryBase<Feedback>
	{
	}
}
