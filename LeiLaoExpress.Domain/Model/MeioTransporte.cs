﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Model
{
    public class MeioTransporte
    {
        public int Id { get; set; }

        public string Nome { get; set; }

		public IList<Lance> Lances { get; set; }
    }
}
