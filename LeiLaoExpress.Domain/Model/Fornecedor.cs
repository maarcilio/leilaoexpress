﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Model
{
    public class Fornecedor
    {
        public string UserId { get; set; }

        public string Nome { get; set; }

        public string Endereco { get; set; }

        public string Telefone { get; set; }

        public string CNPJ { get; set; }

        public virtual IList<Anexo> Anexos { get; set; }

		public virtual IList<Lance> Lances { get; set; }

		public virtual UserApp User { get; set; }


        public Fornecedor()
        {
            this.Anexos = new List<Anexo>();
			this.Lances = new List<Lance>();
        }
    }
}
