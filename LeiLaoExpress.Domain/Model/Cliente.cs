﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Model
{
    public class Cliente
    {
        public string UserId { get; set; }

        public string Nome { get; set; }

        public string Endereco { get; set; }

        public string Telefone { get; set; }

        public string CGC { get; set; }

        public virtual UserApp User { get; set; }
    }
}
