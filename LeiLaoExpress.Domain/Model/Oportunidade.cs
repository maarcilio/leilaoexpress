﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Model
{
    public class Oportunidade
    {
        public int Id { get; set; }

        public virtual string Origem { get; set; }

        public virtual string Destino { get; set; }

        public virtual DateTime DataColeta { get; set; }

        public virtual DateTime DataEntrega { get; set; }

        public string UserId { get; set; }

        public virtual UserApp User { get; set; }

        public virtual IList<Lance> Lances { get; set; }

        public virtual IList<Anexo> Anexos { get; set; }

        public virtual IList<Carga> Cargas { get; set; }

		public virtual IList<Feedback> Feedback { get; set; }

        public Oportunidade()
        {
            this.Anexos = new List<Anexo>();
            this.Lances = new List<Lance>();
            this.Cargas = new List<Carga>();
			this.Feedback = new List<Feedback>();
        }
    }
}
