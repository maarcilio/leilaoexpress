﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeiLaoExpress.Domain.Enum;

namespace LeiLaoExpress.Domain.Model
{
	public class Feedback
	{
		public virtual int Id { get; set; }

		public virtual string Comentario { get; set; }

		public virtual Classificacao Classificacao { get; set; }

		public string FornecedorId { get; set; }

		public virtual UserApp Fornecedor { get; set; }

		public int OportunidadeId { get; set; }

		public virtual Oportunidade Oportunidade { get; set; }
	}
}
