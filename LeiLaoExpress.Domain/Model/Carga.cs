﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Model
{
    public class Carga
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public float Peso { get; set; }

        public string Dimensao { get; set; }

        public bool Fragil { get; set; }

        public virtual IList<Anexo> Anexos { get; set; }

        public virtual IList<Oportunidade> Oportunidades{ get; set; }

		public string UserId { get; set; }

		public virtual UserApp User { get; set; }
            
        public Carga()
        {
            this.Anexos = new List<Anexo>();
            this.Oportunidades = new List<Oportunidade>();
        }
    }
}
