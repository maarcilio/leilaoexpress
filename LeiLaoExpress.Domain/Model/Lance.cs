﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Model
{
    public class Lance
    {
        public int Id { get; set; }

        public decimal Valor { get; set; }

        public DateTime DataColeta { get; set; }

        public DateTime DataEntrega { get; set; }

        public string Observacao { get; set; }

		public bool Escolhida { get; set; }

		public int MeioTransporteId { get; set; }

		public virtual MeioTransporte MeioTransporte { get; set; }

		public string FornecedorId { get; set; }

        public virtual Fornecedor Fornecedor { get; set; }

		public int OportunidadeId { get; set; }

		public virtual Oportunidade Oportunidade { get; set; }

		public Lance()
		{
			Escolhida = false;
		}
    }

}
