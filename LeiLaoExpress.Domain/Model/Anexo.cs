﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Model
{
    public class Anexo
    {
        public int Id { get; set; }

        public string Caminho { get; set; }

        public string Nome { get; set; }

        public virtual IList<Fornecedor> Fornecedores { get; set; }

        public virtual IList<Carga> Cargas { get; set; }

        public virtual IList<Oportunidade> Oportunidades { get; set; }

        public Anexo()
        {
            this.Fornecedores = new List<Fornecedor>();
            this.Cargas = new List<Carga>();
            this.Oportunidades = new List<Oportunidade>();
        }
    }
}
