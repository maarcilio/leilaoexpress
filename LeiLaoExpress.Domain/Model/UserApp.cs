﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LeiLaoExpress.Domain.Enum;

namespace LeiLaoExpress.Domain.Model
{
    public class UserApp : IdentityUser
    {
        public TypeOfUser TypeOfUser { get; set; }

        public virtual Fornecedor Fornecedor { get; set; }
        public virtual Cliente Cliente{ get; set; }

        public virtual IList<Oportunidade> Oportunidades { get; set; }

		public virtual IList<Carga> Cargas { get; set; }

        public virtual IList<Feedback> Feedback { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UserApp> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            var user = await manager.FindByIdAsync(userIdentity.GetUserId());
            // Add custom user claims here
            var claims = new List<Claim>();

            claims.Add(new Claim("TypeOfUser", user.TypeOfUser.ToString()));
            claims.Add(new Claim("EmailConfirm", user.EmailConfirmed.ToString()));

            userIdentity.AddClaims(claims);

            return userIdentity;
        }
    }
}
