﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Domain.Enum
{
    public enum TypeOfClient : int
    {
        [Display(Name = "TypeOfClientPhysical", ResourceType = typeof(Inputs))]
        Fisica = 1,
        [Display(Name = "TypeOfClientLegal", ResourceType = typeof(Inputs))]
        Juridica = 2
    }
}
