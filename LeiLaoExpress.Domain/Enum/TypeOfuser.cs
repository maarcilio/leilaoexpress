﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeiLaoExpress.Common.Resource;

namespace LeiLaoExpress.Domain.Enum
{
    public enum TypeOfUser : int
    {
        [Display(Name = "TypeOfUserCliente", ResourceType = typeof(Inputs))]
        Cliente = 1,
        [Display(Name = "TypeOfUserFornecedor", ResourceType = typeof(Inputs))]
        Fornecedor = 2
    }
}
