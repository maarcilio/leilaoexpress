﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
	public class FeedbackService : ServiceBase<Feedback>, IFeedbackService
	{
		private readonly IFeedbackRepository _repository;

		public FeedbackService(IFeedbackRepository repository)
			: base(repository)
		{
			_repository = repository;
		}
	}
}
