﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
	public class MeioTransporteService : ServiceBase<MeioTransporte>, IMeioTransporteService
	{
		private readonly IMeioTransporteRepository _repository;

		public MeioTransporteService(IMeioTransporteRepository repository)
            : base(repository)
        {
            _repository = repository;
        }
	}
}
