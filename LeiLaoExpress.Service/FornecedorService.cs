﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
    public class FornecedorService : ServiceBase<Fornecedor>, IFornecedorService
    {
        private readonly IFornecedorRepository _repository;

        public FornecedorService(IFornecedorRepository repository)
            :base(repository)
        {
            _repository = repository;
        }
    }
}
