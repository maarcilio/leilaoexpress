﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
    public class AnexoService : ServiceBase<Anexo>,IAnexoService
    {
        private readonly IAnexoRepository _repository;

        public AnexoService(IAnexoRepository repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}
