﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
    public class OportunidadeService : ServiceBase<Oportunidade>, IOportunidadeService
    {
        private readonly IOportunidadeRepository _repository;
        public OportunidadeService(IOportunidadeRepository repository)
            : base(repository)
        {
            _repository = repository;
        }

    }
}
