﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
	public class LanceService : ServiceBase<Lance>, ILanceService
	{
		private readonly ILanceRepository _repository;

		public LanceService(ILanceRepository repository)
            : base(repository)
        {
            _repository = repository;
        }
	}
}
