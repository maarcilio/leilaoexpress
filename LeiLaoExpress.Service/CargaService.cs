﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using LeiLaoExpress.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
    public class CargaService : ServiceBase<Carga>, ICargaService
    {
        private readonly ICargaRepository _repository;

        public CargaService(ICargaRepository repository)
            :base(repository)
        {
            _repository = repository;
        }
    }
}
