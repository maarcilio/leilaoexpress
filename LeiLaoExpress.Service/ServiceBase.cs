﻿using LeiLaoExpress.Domain.Contract.Repository;
using LeiLaoExpress.Domain.Contract.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Service
{
    public class ServiceBase<TEntity> : IDisposable,IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public void Create(TEntity model)
        {
            _repository.Create(model);
        }

        public  void CreateAsync(TEntity model)
        {
            _repository.CreateAsync(model);
        }

        public void Update(TEntity model)
        {
            _repository.Update(model);
        }

        public void UpdateAsync(TEntity model)
        {
            _repository.UpdateAsync(model);
        }

        public void Remove(TEntity model)
        {
            _repository.Remove(model);
        }

        public void RemoveAsync(TEntity model)
        {
            _repository.RemoveAsync(model);
        }

        public void Remove(int Id)
        {
            var model = _repository.GetById(Id);
            this.Remove(model);
        }

        public async void RemoveAsync(int Id)
        {
            var model = await _repository.GetByIdAsync(Id);
            this.RemoveAsync(model);
        }

        public void Remove(Guid Id)
        {
            var model = _repository.GetById(Id);
            this.Remove(model);
        }

        public async void RemoveAsync(Guid Id)
        {
            var model = await _repository.GetByIdAsync(Id);
            this.RemoveAsync(model);
        }

        public TEntity GetById(int Id)
        {
            return _repository.GetById(Id);
        }

        public async Task<TEntity> GetByIdAsync(int Id)
        {
            return await _repository.GetByIdAsync(Id);
        }

        public TEntity GetById(Guid Id)
        {
            return _repository.GetById(Id);
        }

        public async Task<TEntity> GetByIdAsync(Guid Id)
        {
            return await _repository.GetByIdAsync(Id);
        }

        public ICollection<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public async Task<ICollection<TEntity>> GetAllAsync()
        {
            return await _repository.GetAllAsync();
        }

        public ICollection<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.GetAll(predicate);
        }

        public async Task<ICollection<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _repository.GetAllAsync(predicate);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }


        public TEntity GetById(string Id)
        {
            return _repository.GetById(Id);
        }

        public async Task<TEntity> GetByIdAsync(string Id)
        {
            return await _repository.GetByIdAsync(Id);
        }
    }
}
