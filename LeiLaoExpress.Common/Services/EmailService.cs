﻿using LeiLaoExpress.Common.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LeiLaoExpress.Common.Services
{
    public class EmailService
    {
        private string SMTP { get; set; }
        private int Porta { get; set; }
        private string UserName { get; set; }
        private string Pasword { get; set; }

        public EmailService(string smtp, int porta, string username, string password)
        {
            this.SMTP = smtp;
            this.Porta = porta;
            this.UserName = username;
            this.Pasword = password;
        }

        public EmailService()
        {
            this.SMTP = "smtp.mandrillapp.com";
            this.Porta = 587;
            this.UserName = "marciliomac2@gmail.com";
            this.Pasword = "ZQunsamCBJ4bMIwlG7azSw";
        }

        public Task SendEmail(string destination, string subject, string body)
        {
            var msg = new MailMessage();
            msg.From = new MailAddress(this.UserName, Text.NameSystem);
            msg.To.Add(new MailAddress(destination));
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;
            var smtpClient = new SmtpClient(this.SMTP, this.Porta);
            var credentials = new NetworkCredential(this.UserName, this.Pasword);
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);

            return Task.FromResult(0);
        }
    }
}
